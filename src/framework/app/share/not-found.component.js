import {WFMComponent} from "framework";

class NotFound extends WFMComponent{
    constructor(config) {
        super(config);
    }
}

export const notFound = new NotFound({
    selector: 'app-not-found',
    template: `
        <div class="not-found__block">
            <h2 class="not-found__red">Page not found</h2>
            <a href="#">Go to main page</a>
        </div> 
   `,
    styles: `
        .not-found__block {
            display: flex; 
            align-items: center; 
            justify-content: center
        }
        .not-found__red {
            red darken-1;
        }    
    `
});