import {WFMComponent, router, http, _, $} from "framework";
import {brandFormComponent} from "framework/app/pages/brand-form.component";

class BrandsPageComponent extends WFMComponent {
    constructor(config) {
        super(config);
        this.data = {
            tableBody: '',
        };
    }

    onInit() {
        this.data.tableBody = '';
    }

    addListenElems(nameClass, func) {
        var elements = document.querySelectorAll(nameClass);
        for (var i = 0; i < elements.length; i++) {
            elements[i].addEventListener('click', func);
        }
    }

    afterInit() {
        var context = this;
        http.get('http://localhost:81/api/brands')
            .then( ({categories}) => {
                categories.forEach(brand => {
                    this.data.tableBody +=
                        '<tr><td>' + brand.name + '</td><td>' + brand.brand_logo + '</td><td>' + brand.category_id + '</td>'
                        + '<td><a class="js-link-del" data-id="' + brand.id + '" data-name="' + brand.name + '" class="btn-small indigo">remove</a> '
                        + '<a class="js-link-edit" data-logo="' + brand.brand_logo + '" data-category_id="' + brand.category_id + '"  data-id="' + brand.id + '" data-name="' + brand.name + '" class="btn-small indigo">edit</a></td></tr>'
                });
                this.render();

                this.addListenElems('.js-link-edit', function() {
                    brandFormComponent.data.valName = this.dataset.name;
                    brandFormComponent.data.valIdCategory = this.dataset.category_id;
                    brandFormComponent.data.id = this.dataset.id;
                    brandFormComponent.data.valLogo = this.dataset.logo;
                    context.goToBrandEditForm();

                });
                this.addListenElems('.js-link-del', function() {
                    http.delete('http://localhost:81/api/brands/' + this.dataset.id)
                        .then(response => {
                            this.parentNode.parentNode.parentNode.removeChild(this.parentNode.parentNode);
                        });
                });

            })
    }

    events() {
        return {
            'click .js-link': 'goToBrandAddForm',
        }
    }

    goToBrandAddForm() {
        brandFormComponent.data.valName = '';
        brandFormComponent.data.valIdCategory =0;
        brandFormComponent.data.id = 0;
        brandFormComponent.data.valLogo = '';
        brandFormComponent.data.typeForm = 'Add';
        router.navigate('brand-add-form')
    }

    goToBrandEditForm() {
        brandFormComponent.data.typeForm = 'Edit';
        router.navigate('brand-add-form')
    }

}

export const brandsPageComponent = new BrandsPageComponent({
    selector: 'app-brands-page',
    template: ` 
      <h4>Brands</h4>
      <a class="btn-floating waves-effect waves-light indigo js-link"><i class="material-icons">add</i></a>
      <table>
        <thead>
          <tr>
            <th>Name</th>
            <th>Brand Logo</th>
            <th>Id Category</th>
            <th></th>
          </tr>
        </thead>

        <tbody>
            {{ tableBody }}
        </tbody>
      </table>
    `,
    styles: `
        .hidden-link {
            float:left;
            height:0;
            overflow:hidden;
        }
    `
});