import {WFMComponent, router, http, _, $} from "framework";

class ProductFormComponent extends WFMComponent {
    constructor(config) {
        super(config);
        this.data = {
            valName: '',
            valImage: '',
            valBrandId: '',
            typeForm: 'Add',
            id: 0
        };

    }

    events() {
        return {
            'click .js-link': ((this.data.typeForm === 'Add') ? 'addProduct' : 'editProduct')
        }
    }

    addProduct() {
        http.post('http://localhost:81/api/products', {
            name: document.getElementById('name').value,
            image: document.getElementById('image').value,
            brand_id: document.getElementById('brand_id').value

        })
            .then(response => {
                router.navigate('products')
            })
    }

    editProduct() {
        http.put('http://localhost:81/api/products/' + this.data.id, {
            name: document.getElementById('name').value,
            image: document.getElementById('image').value,
            brand_id: document.getElementById('brand_id').value
        })
            .then(response => {
                router.navigate('products')
            })
    }

}

export const productFormComponent = new ProductFormComponent({
    selector: 'app-product-add-form',
    template: ` 
          <div class="row">
            <h4>{{typeForm}}ing of product</h4>
            <form class="col s12">
              <div class="row">
                <div class="input-field col s12">
                  <input placeholder="Name product" id="name" type="text" class="validate" value="{{valName}}">
                </div>
              </div>
              <div class="row">
                <div class="input-field col s12">
                  <input placeholder="Path Image" id="image" type="text" class="validate" value="{{valImage}}">
                </div>
              </div>    
              <div class="row">
                <div class="input-field col s12">
                  <input placeholder="Id Brand" id="brand_id" type="text" class="validate" value="{{valBrandId}}">
                </div>
              </div>                        
            </form>
          </div>
          <a class="btn-floating waves-effect waves-light btn-large indigo js-link">{{typeForm}}</a>
    `,
    styles: `
        
    `
});

