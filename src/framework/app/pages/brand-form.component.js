import {WFMComponent, router, http, _, $} from "framework";

class BrandFormComponent extends WFMComponent {
    constructor(config) {
        super(config);
        this.data = {
            valName: '',
            valIdCategory: 0,
            valLogo: '',
            typeForm: 'Add',
            id: 0
        };

    }

    events() {
        return {
            'click .js-link': ((this.data.typeForm === 'Add') ? 'addBrand' : 'editBrand')
        }
    }

    addBrand() {
        http.post('http://localhost:81/api/brands', {
            name: document.getElementById('name').value,
            brand_logo: document.getElementById('brand_logo').value,
            category_id: document.getElementById('category_id').value
        })
            .then(response => {
                router.navigate('brands')
            })
    }

    editBrand() {
        http.put('http://localhost:81/api/brands/' + this.data.id, {
            name: document.getElementById('name').value,
            brand_logo: document.getElementById('brand_logo').value,
            category_id: document.getElementById('category_id').value
        })
            .then(response => {
                router.navigate('brands')
            })
    }

}

export const brandFormComponent = new BrandFormComponent({
    selector: 'app-brand-add-form',
    template: ` 
          <div class="row">
            <h4>{{typeForm}}ing of brand</h4>
            <form class="col s12">
              <div class="row">
                <div class="input-field col s12">
                  <input placeholder="Name brand" id="name" type="text" class="validate" value="{{valName}}">
                </div>
              </div>
              <div class="row">
                <div class="input-field col s12">
                  <input placeholder="Path Logo" id="brand_logo" type="text" class="validate" value="{{valLogo}}">
                </div>
              </div>
              <div class="row">
                <div class="input-field col s12">
                  <input placeholder="Id Category" id="category_id" type="text" class="validate" value="{{valIdCategory}}">
                </div>
              </div>              
            </form>
          </div>
          <a class="btn-floating waves-effect waves-light btn-large indigo js-link">{{typeForm}}</a>
    `,
    styles: `
        
    `
});

