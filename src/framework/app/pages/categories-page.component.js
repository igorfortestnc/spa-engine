import {WFMComponent, router, http, _, $} from "framework";
import {categoryFormComponent} from "framework/app/pages/category-form.component";

class CategoriesPageComponent extends WFMComponent {
    constructor(config) {
        super(config);
        this.data = {
            tableBody: '',
        };
    }

    onInit() {
        this.data.tableBody = '';
    }

    addListenElems(nameClass, func) {
        var elements = document.querySelectorAll(nameClass);
        for (var i = 0; i < elements.length; i++) {
            elements[i].addEventListener('click', func);
        }
    }

    afterInit() {
        var context = this;
        http.get('http://localhost:81/api/categories')
            .then( ({categories}) => {
                categories.forEach(category => {
                    this.data.tableBody +=
                        '<tr><td>' + category.name + '</td><td>' + category.ids + '</td><td>'
                        + '<a class="js-link-del" data-id="' + category.id + '" data-name="' + category.name + '"class="btn-small indigo">remove</a> '
                        + '<a class="js-link-edit" data-id="' + category.id + '" data-name="' + category.name + '"class="btn-small indigo">edit</a></td></tr>'
                });
                this.render();

                this.addListenElems('.js-link-edit', function() {
                    categoryFormComponent.data.valName = this.dataset.name;
                    categoryFormComponent.data.idCategory = this.dataset.id;
                    context.goToСategoryEditForm();

                });
                this.addListenElems('.js-link-del', function() {
                    http.delete('http://localhost:81/api/categories/' + this.dataset.id)
                        .then(response => {
                            this.parentNode.parentNode.parentNode.removeChild(this.parentNode.parentNode);
                        });
                });

            })
    }

    events() {
        return {
            'click .js-link': 'goToСategoryAddForm',
        }
    }

    goToСategoryAddForm() {
        categoryFormComponent.data.valName = '';
        categoryFormComponent.data.typeForm = 'Add';
        router.navigate('category-add-form')
    }

    goToСategoryEditForm() {
        categoryFormComponent.data.typeForm = 'Edit';
        router.navigate('category-add-form')
    }

}

export const categoriesPageComponent = new CategoriesPageComponent({
    selector: 'app-categories-page',
    template: ` 
      <h4>Categories</h4>
      <a class="btn-floating waves-effect waves-light indigo js-link"><i class="material-icons">add</i></a>
      <table>
        <thead>
          <tr>
            <th>Name</th>
            <th>Ids</th>
            <th></th>
          </tr>
        </thead>

        <tbody>
            {{ tableBody }}
        </tbody>
      </table>
    `,
    styles: `
        .hidden-link {
            float:left;
            height:0;
            overflow:hidden;
        }
    `
});