import {WFMComponent, router, http, _, $} from "framework";
import {productFormComponent} from "framework/app/pages/product-form.component";

class ProductsPageComponent extends WFMComponent {
    constructor(config) {
        super(config);
        this.data = {
            tableBody: '',
        };
    }

    onInit() {
        this.data.tableBody = '';
    }

    addListenElems(nameClass, func) {
        var elements = document.querySelectorAll(nameClass);
        for (var i = 0; i < elements.length; i++) {
            elements[i].addEventListener('click', func);
        }
    }

    afterInit() {
        var context = this;
        http.get('http://localhost:81/api/products')
            .then( ({categories}) => {
                categories.forEach(product => {
                    this.data.tableBody +=
                        '<tr><td>' + product.name + '</td><td>' + product.image + '</td><td>' + product.brand_id + '</td>'
                        + '<td><a class="js-link-del" data-id="' + product.id + '" data-name="' + product.brand_id + '" class="btn-small indigo">remove</a> '
                        + '<a class="js-link-edit" data-image="' + product.image + '" data-brand_id="' + product.brand_id + '"  data-id="' + product.id + '" data-name="' + product.name + '" class="btn-small indigo">edit</a></td></tr>'
                });
                this.render();

                this.addListenElems('.js-link-edit', function() {
                    productFormComponent.data.valName = this.dataset.name;
                    productFormComponent.data.id = this.dataset.id;
                    productFormComponent.data.valBrandId = this.dataset.brand_id;
                    productFormComponent.data.valImage = this.dataset.image;
                    context.goToProductEditForm();

                });
                this.addListenElems('.js-link-del', function() {
                    http.delete('http://localhost:81/api/products/' + this.dataset.id)
                        .then(response => {
                            this.parentNode.parentNode.parentNode.removeChild(this.parentNode.parentNode);
                        });
                });

            })
    }

    events() {
        return {
            'click .js-link': 'goToProductAddForm',
        }
    }

    goToProductAddForm() {
        productFormComponent.data.valName = '';
        productFormComponent.data.valBrandId = 0;
        productFormComponent.data.valImage = '';
        productFormComponent.data.typeForm = 'Add';
        router.navigate('product-add-form')
    }

    goToProductEditForm() {
        productFormComponent.data.typeForm = 'Edit';
        router.navigate('product-add-form')
    }
}

export const productsPageComponent = new ProductsPageComponent({
    selector: 'app-products-page',
    template: `
      <h4>Products</h4>
      <a class="btn-floating waves-effect waves-light indigo js-link"><i class="material-icons">add</i></a>
      <table>
        <thead>
          <tr>
            <th>Name</th>
            <th>Image path</th>
            <th>Brand Id</th>
            <th></th>
          </tr>
        </thead>

        <tbody>
            {{ tableBody }}
        </tbody>
      </table>
    `,
    styles: `
        .hidden-link {
            float:left;
            height:0;
            overflow:hidden;
        }
    `
});