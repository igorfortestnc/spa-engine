import {WFMComponent, router, http, _, $} from "framework";

class CategoryFormComponent extends WFMComponent {
    constructor(config) {
        super(config);
        this.data = {
            valName: '',
            typeForm: 'Add',
            idCategory: 0
        };

    }

    events() {
        return {
            'click .js-link': ((this.data.typeForm === 'Add') ? 'addCategory' : 'editCategory')
        }
    }

    addCategory() {
        http.post('http://localhost:81/api/categories', {name: document.getElementById('name').value})
            .then(response => {
                router.navigate('categories')
            })
    }

    editCategory() {
        http.put('http://localhost:81/api/categories/' + this.data.idCategory, {name: document.getElementById('name').value})
            .then(response => {
                router.navigate('categories')
            })
    }

}

export const categoryFormComponent = new CategoryFormComponent({
    selector: 'app-category-add-form',
    template: ` 
          <div class="row">
            <h4>{{typeForm}}ing of category</h4>
            <form class="col s12">
              <div class="row">
                <div class="input-field col s12">
                  <input placeholder="Name category" id="name" type="text" class="validate" value="{{valName}}">
                </div>
              </div>
            </form>
          </div>
          <a class="btn-floating waves-effect waves-light btn-large indigo js-link">{{typeForm}}</a>
    `,
    styles: `
        
    `
});

