import {notFound} from "./share/not-found.component";
import {categoriesPageComponent} from "framework/app/pages/categories-page.component";
import {categoryFormComponent} from "framework/app/pages/category-form.component";
import {brandsPageComponent} from "framework/app/pages/brands-page.component";
import {brandFormComponent} from "framework/app/pages/brand-form.component";
import {productsPageComponent} from "framework/app/pages/products-page.component";
import {productFormComponent} from "framework/app/pages/product-form.component";

export const appRoutes = [
    { path: '**', component: notFound },
    { path: 'categories', component: categoriesPageComponent },
    { path: 'category-add-form', component: categoryFormComponent },
    { path: 'brands', component: brandsPageComponent },
    { path: 'brand-add-form', component: brandFormComponent },
    { path: 'products', component: productsPageComponent },
    { path: 'product-add-form', component: productFormComponent },
];