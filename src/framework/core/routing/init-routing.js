import {RoutingModule} from "framework/core/routing/routing.module";
import {_} from "framework/index";

export function initRouting(routes, dispatcher) {
    if (_.isUndefined(routes)) {
        return
    }
    let routing = new RoutingModule(routes, dispatcher);
    routing.init();
}