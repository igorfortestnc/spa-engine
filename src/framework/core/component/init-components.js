import {renderComponent} from "framework/core/component/render-component";
import {_} from "framework/index";

export function initComponents(bootstrap, components) {

    if (_.isUndefined(bootstrap)) {
        throw new Error(`Bootstrap component is undefined`)
    }

    [bootstrap, ...components].forEach(renderComponent);
}