class Http {
    get(url) {
       return sendRequest('GET', url)
    }

    post(url, data) {
        return sendRequest('POST', url, data)
    }

    put(url, data) {
        return sendRequest('PUT', url, data)
    }

    delete(url) {
        return sendRequest('DELETE', url)
    }
}

function sendRequest(method, url, data = {}) {
    let init = {
        method: method,
        headers: {
         'Accept':       'application/json',
         'Content-Type': 'application/x-www-form-urlencoded',
        },
    };
    if ((method !== 'GET') && (method !== 'HEAD')) {
        init.body = JSON.stringify(data);
    }
    return fetch(url, init).then(response => response.json())
}

export const http = new Http();