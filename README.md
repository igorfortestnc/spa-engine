spa-engine - component-orientired aplication support fetch-rest,  realised with pure js. 
Pages for rest request is  here src/framework/app/pages


{
  "categories": 
  [
	{
		"id": "57b42bfe31b6f0132cb96836",
		"name": "Mobile phones",
		"brands": 
		[
			{
				"id": 1,
				"name": "Samsung",
				"brand_logo": "",
				"products": 
				[
					{
						"id": 12345,
						"name": "Galaxy S4",
						"image": ""
					},
					{
						"id": 23456,
						"name": "Galaxy S5",
						"image": ""
					},
					{
						"id": 34567,
						"name": "Galaxy S6",
						"image": ""
					}
				]
			},
			{
				"id": 2,
				"name": "Sony",
				"brand_logo": "",
				"products": 
				[
					{
						"id": 45678,
						"name": "XPeria Z3",
						"image": ""
					},
					{
						"id": 56789,
						"name": "XPeria Z5",
						"image": ""
					},
					{
						"id": 67890,
						"name": "Vegas",
						"image": ""
					}
				]
			},
			{
				"id": 3,
				"name": "Huawei",
				"brand_logo": "",
				"products": [{
					"id": 44444,
					"name": "Honor 7",
					"image": ""
				},
				{
					"id": 55555,
					"name": "P8",
					"image": ""
				},
				{
					"id": 66666,
					"name": "P9",
					"image": ""
				}]
			},
			{
				"id": 4,
				"name": "Apple",
				"brand_logo": "",
				"products": [{
					"id": 12222,
					"name": "IPhone 5S",
					"image": ""
				},
				{
					"id": 23333,
					"name": "IPhone 6S",
					"image": ""
				}]
			},
			{
				"id": 5,
				"name": "Microsoft",
				"brand_logo": "",
				"products": [{
					"id": 543534,
					"name": "Lumia 650",
					"image": ""
				},
				{
					"id": 456457,
					"name": "Lumia 630",
					"image": ""
				},
				{
					"id": 7564534,
					"name": "Lumia 640 XL",
					"image": ""
				}]
			}
		]
	},
	{
		"id": "57b42bfe7e7298611b333652",
		"name": "Computers",
		"brands": [{ //products
			"id": 1,
			"name": "Samsung",
			"brand_logo": "",
			"products": []
		},
		{
			"id": 2,
			"name": "Sony",
			"products": [{
				"id": 7564534,
				"name": "Sony Vaio",
				"image": ""
			}]
		},
		{
			"id": 3,
			"name": "Apple",
			"brand_logo": "",
			"products": [{
				"id": 7560001,
				"name": "MacBook Pro",
				"image": ""
			},
			{
				"id": 665451,
				"name": "MacBook Air",
				"image": ""
			}]
		},
		{
			"id": 4,
			"name": "Microsoft",
			"brand_logo": "",
			"products": []
		}]
	},
	{
		"id": "57b42bfe250111078dadcd03",
		"name": "Cameras",
		"brands": [{ 
			"id": 1,
			"name": "Canon",
			"brand_logo": "",
			"products": [{
				"id": 345611,
				"name": "EOS 1000D",
				"image": ""
			}]
		}]
	}
  ]
}